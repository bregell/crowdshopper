/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.crowdshopper;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.measure.quantity.Mass;
import org.jboss.arquillian.junit.Arquillian;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Johan
 */
@RunWith(Arquillian.class)
public class ShopTest extends TestBase{
    
    private Shop shop;
    private CSUser user;
    private Recipe recipe;
    private List<Item> items;
    private Map<Item, Measure> measurements;
    private Map<Item, Date> lastBought;
    
    private void setup(){
        //Create Shop
        shop = new Shop("Lidl", new Address("Lidlgatan", "40A", 41522));
        shop = main.getShopCatalogue().update(shop);
        
        //Create User
        user = new CSUser("Test", "password", new Address("Gatan", "40B", 41522));
        user = main.getUserCatalogue().update(user);
        
        //Create Recipe with items and add them to Shop inventory
        recipe = new Recipe("Mat", 2, "God mat", "Laga den", user);
        recipe = main.getRecipeCatalogue().update(recipe);
        items = new LinkedList<>();
        measurements = new HashMap<>();
        lastBought = new HashMap<>();
        for(int i = 0; i < 10; i++){
            Item item = main.getItemCatalogue().update(new Item("Item "+Integer.toString(i)));
            //Item item = new Item("Item "+Integer.toString(i));
            items.add(item);
            shop.getInventory().addItem(item);
            shop.getInventory().setPrice(item, (double) i);
            Measure m = new Measure((double) i, Mass.UNIT);
            shop.getInventory().setMeasurement(item, m);
            measurements.put(item, m);
            lastBought.put(item, new DateTime(2014, 10, 10, 10, 30).toDate());
        }
        shop = main.getShopCatalogue().update(shop);
        //Populate Recipe
        recipe.setItems(items);
        recipe = main.getRecipeCatalogue().update(recipe);
        recipe.setMeasurements(measurements);
        recipe = main.getRecipeCatalogue().update(recipe);   
        //Populate User Inventory
        user.getInventory().setItems(items);
        //user.getInventory().setMeasurements(measurements);
        //user.getInventory().setLastBought(lastBought);
        user = main.getUserCatalogue().update(user);
        for(Item item : user.getInventory().getItems()){
            user.getInventory().addMeasure(item, new Measure((double) 15, Mass.UNIT), new DateTime(2014, 10, 15, 10, 30).toDate());
        }
        user = main.getUserCatalogue().update(user);
    }
    
    @Test public void presistShop(){
        setup();
        Assert.assertTrue("Shop not created", main.getShopCatalogue().count() == 1);
        Assert.assertTrue("Items not added to ShopInventory", main.getShopCatalogue().findAll().get(0).getInventory().size() == 10);
        Assert.assertTrue("Measures not added to ShopInventory", !main.getShopCatalogue().findAll().get(0).getInventory().getMeasurements().isEmpty());
        Assert.assertTrue("Avreage Prices not added to ShopInventory", !main.getShopCatalogue().findAll().get(0).getInventory().getAvgPrices().isEmpty());
        Assert.assertTrue("Current Prices not added to ShopInventory", !main.getShopCatalogue().findAll().get(0).getInventory().getPrices().isEmpty());  
        main.getShopCatalogue().delete(shop.getId());
        Assert.assertTrue("Shop not deleted", main.getShopCatalogue().count() == 0);
        Assert.assertTrue("Items not deleted from ShopInventory", em.createQuery("SELECT count(si) FROM ShopInventory si", Long.class).getSingleResult() == 0.0);
        Assert.assertTrue("Measures not deleted from ShopInventory", em.createQuery("SELECT count(si.measurements) FROM ShopInventory si", Long.class).getSingleResult() == 0.0);
        Assert.assertTrue("Avreage Prices not deleted from ShopInventory", em.createQuery("SELECT count(si.avgPrices) FROM ShopInventory si", Long.class).getSingleResult() == 0.0);
        Assert.assertTrue("Current Prices not deleted from ShopInventory", em.createQuery("SELECT count(si.prices) FROM ShopInventory si", Long.class).getSingleResult() == 0.0);
    }
    
    @Test public void shopFunctions(){
        setup();
        Assert.assertTrue("Shop not created", main.getShopCatalogue().count() == 1);
        Assert.assertTrue("Recpie not created", main.getRecipeCatalogue().count() == 1);
        Assert.assertTrue("User not created", main.getUserCatalogue().count() == 1);
        ShoppingList shoppingList = new ShoppingList();
        shoppingList = main.getShoppingListsCatalogue().update(shoppingList);
        main.getUserCatalogue().generateShoppingList(user, shoppingList);
        user = main.getUserCatalogue().update(user);
        main.getShoppingListsCatalogue().addRecipe(user.getShoppinglists().iterator().next(), recipe);
        Map<Item, Double> prices = main.getShopCatalogue().getShoppingListPrice(shop, user.getShoppinglists().iterator().next());
        int i = 0;
        for(Map.Entry<Item,Double> price: prices.entrySet()){         
            Assert.assertTrue("Price not found", price.getValue() != null);
        }
    }
}
