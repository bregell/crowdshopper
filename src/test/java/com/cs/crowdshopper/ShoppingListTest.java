/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.crowdshopper;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.measure.quantity.Mass;
import javax.measure.quantity.Volume;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Johan
 */
@RunWith(Arquillian.class)
public class ShoppingListTest extends TestBase{
    final String name;
    final ShoppingList shoppingList;

    public ShoppingListTest() {
        this.name = "Test List";
        this.shoppingList = new ShoppingList();
    }
    
    @Test public void createShoppingList(){
        shoppingList.setName(name);
        main.getShoppingListsCatalogue().create(shoppingList);
        ShoppingList dbShoppingList = main.getShoppingListsCatalogue().findAll().get(0);
        Assert.assertTrue("ShoppingList not created", dbShoppingList.getName().equals(name));
    }
    
    @Test public void deleteShoppingList(){
        Assert.assertTrue("ShoppingList list null empty", main.getShoppingListsCatalogue().count() == 0);
        main.getShoppingListsCatalogue().create(shoppingList);
        Assert.assertTrue("ShoppingList list count not 1", main.getShoppingListsCatalogue().count() == 1);
        main.getShoppingListsCatalogue().delete(main.getShoppingListsCatalogue().findAll().get(0).getId());
        Assert.assertTrue("ShoppingList list count not at zero", main.getShoppingListsCatalogue().count() == 0);
    }
    
    @Test public void addItems(){
        main.getShoppingListsCatalogue().create(shoppingList);
        ShoppingList dbShoppingList = main.getShoppingListsCatalogue().findAll().get(0);
        Item meat = new Item("Meat");
        Item spaghetti = new Item("spaghetti");
        dbShoppingList.addItem(meat);
        dbShoppingList.addItem(spaghetti);
        
        main.getShoppingListsCatalogue().update(dbShoppingList);
        dbShoppingList = main.getShoppingListsCatalogue().findAll().get(0);
        
        Iterator<Item> itemsIterator = dbShoppingList.getItems().iterator();
        dbShoppingList.setMeasurement(itemsIterator.next(), new Measure(5.0, Mass.UNIT));
        dbShoppingList.setMeasurement(itemsIterator.next(), new Measure(5.0, Mass.UNIT));
        main.getShoppingListsCatalogue().update(dbShoppingList);
        
        dbShoppingList = main.getShoppingListsCatalogue().findAll().get(0);
        Assert.assertTrue("Items not added to ShoppingList", dbShoppingList.getItems().size() == 2);
        List<Item> items = new LinkedList<>(dbShoppingList.getItems());
        Map<Item, Measure> amounts = dbShoppingList.getMeasurements();
        for(Item i: items){
            Measure amount = amounts.get(i);
            Assert.assertTrue("Wrong Unit", amount.getUnit().equals(Mass.UNIT));
            Assert.assertTrue("Wrong Amount", (amount.getAmount().equals(5.0)));
        } 
    }
    
    @Test public void removeItems(){
        //Create shopping list
        main.getShoppingListsCatalogue().create(shoppingList);
        ShoppingList dbShoppingList = main.getShoppingListsCatalogue().findAll().get(0);
        
        //Insert Items
        dbShoppingList.addItem(new Item("Meat"));
        dbShoppingList.addItem(new Item("spaghetti"));    
        main.getShoppingListsCatalogue().update(dbShoppingList);
        
        //Insert measures
        dbShoppingList = main.getShoppingListsCatalogue().findAll().get(0);
        Iterator<Item> itemsIterator = dbShoppingList.getItems().iterator();
        dbShoppingList.setMeasurement(itemsIterator.next(), new Measure(5.0, Mass.UNIT));
        dbShoppingList.setMeasurement(itemsIterator.next(), new Measure(5.0, Mass.UNIT));
        main.getShoppingListsCatalogue().update(dbShoppingList);
        
        //Check sizes
        dbShoppingList = main.getShoppingListsCatalogue().findAll().get(0);      
        Assert.assertTrue("There is not two items in the ShoppingList", dbShoppingList.getItems().size() == 2);
        Assert.assertTrue("The Measure was not inseted", dbShoppingList.getMeasurements().size() == 2);
        
        //Remove item and measure
        dbShoppingList.removeItem(main.getShoppingListsCatalogue().findAll().get(0).getItems().iterator().next());
        main.getShoppingListsCatalogue().update(dbShoppingList);
        
        //Check sizes
        dbShoppingList = main.getShoppingListsCatalogue().findAll().get(0);
        Assert.assertTrue("There is not one item in the ShoppingList", dbShoppingList.getItems().size() == 1);
        Assert.assertTrue("The Measure was not deleted", dbShoppingList.getMeasurements().size() == 1);
    }
    
    @Test public void changeMeasurement(){
        main.getShoppingListsCatalogue().create(shoppingList);
        ShoppingList dbShoppingList = main.getShoppingListsCatalogue().findAll().get(0);
        Logger.getAnonymousLogger().log(Level.INFO, dbShoppingList.getId().toString());
        dbShoppingList.addItem(new Item("Meat"));
        main.getShoppingListsCatalogue().update(dbShoppingList);
        dbShoppingList = main.getShoppingListsCatalogue().findAll().get(0);
        Logger.getAnonymousLogger().log(Level.INFO, dbShoppingList.getItems().toString());
        dbShoppingList.setMeasurement(dbShoppingList.getItems().iterator().next(), new Measure(10.0, Mass.UNIT));        
        main.getShoppingListsCatalogue().update(dbShoppingList);
        dbShoppingList = main.getShoppingListsCatalogue().findAll().get(0);  
        Assert.assertTrue(
                "Measure is not set correctly", 
                dbShoppingList.getMeasurement(dbShoppingList.getItems().iterator().next()).getUnit().equals(Mass.UNIT)
                        && dbShoppingList.getMeasurement(dbShoppingList.getItems().iterator().next()).getAmount().equals(10.0));
        dbShoppingList.setMeasurement(dbShoppingList.getItems().iterator().next(), new Measure(5.5, Volume.UNIT));
        main.getShoppingListsCatalogue().update(dbShoppingList);
        dbShoppingList = main.getShoppingListsCatalogue().findAll().get(0); 
        Assert.assertTrue("Measure is not correct efter change", 
                dbShoppingList.getMeasurement(dbShoppingList.getItems().iterator().next()).getUnit().equals(Volume.UNIT) 
                        && dbShoppingList.getMeasurement(dbShoppingList.getItems().iterator().next()).getAmount().equals(5.5));  
        
    }           
    
}
