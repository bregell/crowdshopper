/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.crowdshopper;

import org.jboss.arquillian.junit.Arquillian;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.assertTrue;

/**
 *
 * @author Johan
 */
@RunWith(Arquillian.class)
public class ItemTest extends TestBase{
    
    @Test
    public void createItem() throws Exception{
        String name = "Test Item";
        Item item = new Item(name);
        Integer count;
        count = main.getItemCatalogue().count();
        assertTrue("Item collection not null", count == 0);
        main.getItemCatalogue().create(item);
        count = main.getItemCatalogue().count();
        assertTrue("Item collection not 1", count == 1);
        assertTrue("Item not persisted", main.getItemCatalogue().getByName(name).equals(item));
    }
    
    @Test
    public void deleteItem() throws Exception{
        String name = "Test Item";
        Item item = new Item(name);
        Integer count;
        count = main.getItemCatalogue().count();
        assertTrue("Item collection not null", count == 0);
        main.getItemCatalogue().create(item);
        count = main.getItemCatalogue().count();
        assertTrue("Item collection not 1", count == 1);
        Item dbItem = main.getItemCatalogue().getByName(name);
        main.getItemCatalogue().delete(dbItem.getId());
        assertTrue("Item not deleated", main.getItemCatalogue().count() == 0);
    }
    
    @Test
    public void changeItemName() throws Exception{
        String name = "Test Item";
        Item item = new Item(name);
        main.getItemCatalogue().create(item);
        item = main.getItemCatalogue().getByName(name);
        String newName = "New Name";
        item.setName(newName);
        main.getItemCatalogue().update(item);
        assertTrue("Name not changed", main.getItemCatalogue().find(item.getId()).getName().equals(newName));
    }
}
