/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.crowdshopper;

import java.util.LinkedList;
import javax.measure.unit.NonSI;
import junit.framework.Assert;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Johan
 */
@RunWith(Arquillian.class)
public class RecipeTest extends TestBase{
    
    private Recipe recipe;

    public RecipeTest() {
        
    }

    @Before public void resetRecipe(){
        this.recipe = null;
    }
    
    @Test public void persistRecipe(){
        recipe = new Recipe("Test", 4, "Good food", "Cook it stupid");
        Assert.assertTrue("Recipes not empty", 0 == main.getRecipeCatalogue().count());
        recipe = main.getRecipeCatalogue().update(recipe);
        Assert.assertTrue("Recpie not created", 1 == main.getRecipeCatalogue().count());
        main.getRecipeCatalogue().delete(recipe.getId());
        Assert.assertTrue("Recpie not deleted", 0 == main.getRecipeCatalogue().count());
    }
    
    @Test public void persistItems(){
        recipe = new Recipe("Test", 4, "Good food", "Cook it stupid", new CSUser("Test", "Test", new Address("Siriusgatan 74", 41522)));
        Assert.assertTrue("Recipes not empty", 0 == main.getRecipeCatalogue().count());
        recipe = main.getRecipeCatalogue().update(recipe);
        Assert.assertTrue("Recpie not returned by update", recipe.getId() != null);
        Assert.assertTrue("Recpie not created", 1 == main.getRecipeCatalogue().count());
        Item item = main.getItemCatalogue().update(new Item("item 1"));
        recipe.addItem(item);
        recipe.setMeasurement(item, new Measure(1.0, NonSI.BAR));
        recipe = main.getRecipeCatalogue().update(recipe);
        Assert.assertTrue("Item not added", recipe.getItems().contains(item)&& !main.getRecipeCatalogue().findAll().get(0).getItems().isEmpty());
        recipe.removeItem(item);
        recipe = main.getRecipeCatalogue().update(recipe);
        Assert.assertTrue("Item not removed from Recipe", !recipe.getItems().contains(item) && main.getRecipeCatalogue().findAll().get(0).getItems().isEmpty());
        Assert.assertTrue("Item removed from Item ", main.getItemCatalogue().count() == 1);
    }
}
