/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.crowdshopper;

import java.util.Collection;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Before;

/**
 *
 * @author Johan
 */
public class TestBase {
    
    @Inject IMain main;
    
    @Inject UserTransaction utx;
    
    // Need a standalone em to remove testdata between tests
    // No em accessible from interfaces
    @PersistenceContext(unitName = "com.cs_CrowdShopper2_war_1.0-SNAPSHOTPU")
    EntityManager em;

    @Deployment
    public static Archive<?> createDeployment() {
        return ShrinkWrap.create(WebArchive.class, "CS.war")
                .addPackage("com.cs.crowdshopper")
                .addAsResource("META-INF/persistence.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
    } 
    
    @Before  // Run before each test
    public void before() throws Exception {
        clearAll();
    }
    
    private void clearAll() throws Exception {  
        utx.begin();  
        em.joinTransaction();  
        em.createQuery("DELETE FROM HomeInventory").executeUpdate();
        em.createQuery("DELETE FROM ShopInventory").executeUpdate();       
        em.createQuery("DELETE FROM ShoppingList").executeUpdate();
        em.createQuery("DELETE FROM Recipe").executeUpdate();
        em.createQuery("DELETE FROM ItemList").executeUpdate();
        em.createQuery("DELETE FROM Shop").executeUpdate();
        em.createQuery("DELETE FROM Item").executeUpdate(); 
        em.createQuery("DELETE FROM CSUser").executeUpdate();
        utx.commit();
    }
}
