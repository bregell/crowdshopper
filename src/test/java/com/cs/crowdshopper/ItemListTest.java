/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.crowdshopper;

import com.sun.media.jfxmedia.logging.Logger;
import java.util.LinkedList;
import java.util.List;
import org.jboss.arquillian.junit.Arquillian;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Johan
 */
@RunWith(Arquillian.class)
public class ItemListTest extends TestBase{
    
    @Test
    public void createList() throws Exception{
        Integer count;
        count = main.getItemListCatalogue().count();
        assertTrue("Item collection not null", count == 0);
        main.getItemListCatalogue().create(new ItemList());
        count = main.getItemListCatalogue().count();
        assertTrue("Item collection not 1", count == 1);
    }
    
    @Test
    public void deleteList() throws Exception{
        Integer count;
        count = main.getItemListCatalogue().count();
        assertTrue("ItemList collection not null", count == 0);
        main.getItemListCatalogue().create(new ItemList());
        count = main.getItemListCatalogue().count();
        assertTrue("ItemList collection not 1", count == 1);
        List<ItemList> itemLists = main.getItemListCatalogue().findAll();
        main.getItemListCatalogue().delete(itemLists.get(0).getId());
        count = main.getItemListCatalogue().count();
        assertTrue("ItemList not deleted", count == 0);
    }
    
    @Test
    public void populateList() throws Exception{
        Item item;
        main.getItemListCatalogue().create(new ItemList());
        ItemList itemList = main.getItemListCatalogue().findAll().get(0);
        List<Item> items = new LinkedList<>();
        for(int i = 0; i < 10; i++){
            item = new Item(String.valueOf(i));
            itemList.addItem(item);
            items.add(item);
        }
        main.getItemListCatalogue().update(itemList);
        ItemList dbList = main.getItemListCatalogue().find(itemList.getId());
        assertTrue("Items not persisted", dbList.size() == items.size());
    }
    
    @Test
    public void cascadeTest() throws Exception{
        for(int i = 0; i < 10; i++){
            main.getItemCatalogue().create(new Item(Integer.toString(i)));
        }
        main.getItemListCatalogue().create(new ItemList());
        ItemList itemList = main.getItemListCatalogue().findAll().get(0);
        List<Item> items = main.getItemCatalogue().findRange(0, 5);
        for(Item i: items){
            itemList.addItem(i);
        }
        main.getItemListCatalogue().update(itemList);
        itemList = main.getItemListCatalogue().findAll().get(0);
        main.getItemListCatalogue().delete(itemList.getId());
        assertTrue("Too many Items deleated", main.getItemCatalogue().count() == 10);
    }
}
