/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.crowdshopper;

import javax.measure.quantity.Mass;
import junit.framework.Assert;
import org.jboss.arquillian.junit.Arquillian;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Johan
 */

@RunWith(Arquillian.class)
public class CSUserTest extends TestBase{
    private CSUser user;
    
    @Before public void resetUser(){
        user = new CSUser("Test", "password", new Address("Gatan", "40B", 41522));
    }
    
    @Test public void presistUser(){
        Assert.assertTrue("User table not empty", main.getUserCatalogue().count() == 0);
        user = main.getUserCatalogue().update(user);
        Assert.assertTrue("User not created", main.getUserCatalogue().count() == 1);
        user.setPassword("gurka");
        user = main.getUserCatalogue().update(user);
        Assert.assertTrue("User password created", user.getPassword().equals("gurka") && main.getUserCatalogue().findAll().get(0).getPassword().equals("gurka"));
        main.getUserCatalogue().delete(user.getId());
        Assert.assertTrue("User not deleted", main.getUserCatalogue().count() == 0);  
    }
    
    @Test public void inventoryTest(){
        user = main.getUserCatalogue().update(user);
        user.getInventory().addItem(new Item("Gurka"));
        user = main.getUserCatalogue().update(user);
        user.getInventory().addMeasure(user.getInventory().getItems().iterator().next(), new Measure(7.0, Mass.UNIT), new DateTime(2014, 10, 10, 10, 30).toDate());
        user = main.getUserCatalogue().update(user);
        //Logger.getAnonymousLogger().log(Level.INFO, user.getInventory().getAmountPerDay(user.getInventory().getItems().get(0)).getAmount().toString());
        Assert.assertTrue("Amount per day not correct", user.getInventory().getAmountPerDay(user.getInventory().getItems().iterator().next()).getAmount() == 1.0);
        user.getInventory().updateMeasure(main.getItemCatalogue().getByName("Gurka"), new Measure(16.0, Mass.UNIT), new DateTime(2014, 10, 14, 10, 30).toDate());
        user = main.getUserCatalogue().update(user);
        //Logger.getAnonymousLogger().log(Level.INFO, user.getInventory().getAmountPerDay(user.getInventory().getItems().get(0)).getAmount().toString());
        Assert.assertTrue("Amount per day not correct", user.getInventory().getAmountPerDay(user.getInventory().getItems().iterator().next()).getAmount() == 2.0);
        ShoppingList shoppingList = main.getUserCatalogue().generateShoppingList(user, new ShoppingList());
        Assert.assertTrue("Amount in shoppinglist is not correct", shoppingList.getMeasurement(user.getInventory().getItems().iterator().next()).getAmount() == 2.0 * (double) Days.daysBetween(new DateTime(2014, 10, 14, 10, 30), new DateTime()).getDays());
    }
    
    @Test public void persistFavorites(){
        user = main.getUserCatalogue().update(user);
        user.addRecipe(new Recipe("Test", 4, "Mat", "Laga", user));
        user.addShop(new Shop("Lidl", new Address("Lidelgatan", 1234)));
        user.addShoppingList(new ShoppingList("Test"));
        user = main.getUserCatalogue().update(user);
        
        Assert.assertTrue("Recpie not created", user.getFavoriteRecipes().size() == 1);
        Assert.assertTrue("Shop not created", user.getFavoriteShops().size() == 1);
        Assert.assertTrue("ShoppingList not created", user.getFavoriteShops().size() == 1);
        
        user.removeRecipe(user.getFavoriteRecipes().iterator().next());
        user.removeShop(user.getFavoriteShops().iterator().next());
        user.removeShoppingList(user.getShoppinglists().iterator().next());
        
        user = main.getUserCatalogue().update(user);
        
        Assert.assertTrue("Recpie not removed", user.getFavoriteRecipes().isEmpty());
        Assert.assertTrue("Shop not removed", user.getFavoriteShops().isEmpty());
        Assert.assertTrue("ShoppingList not removed", user.getShoppinglists().isEmpty());
        
        Assert.assertTrue("Recipe removed from Recipe database", main.getShopCatalogue().count() == 1);
        Assert.assertTrue("Shop removed from Shop database", main.getRecipeCatalogue().count() == 1);
        Assert.assertTrue("Shop not removed from Shop database", main.getShoppingListsCatalogue().count() == 0);
    }
}
