/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.view;

import com.cs.core.SingletonBean;
import com.cs.crowdshopper.Item;
import com.cs.crowdshopper.Measure;
import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.measure.unit.Unit;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

/**
 *
 * @author iKotsos
 */
@Named
@ViewScoped
public class AddReceiptBB implements Serializable {

    @Size(min = 2, message = "{name}")
    private String shopName;
    @Size(min = 2, message = "{item.name}")
    private String newItemName;
    @Min(value = 1, message = "{item.amount}")
    private String newItemAmount;
    private String newItemUnit;
    @Min(value = 1, message = "{item.price}")
    private String newItemPrice;
    private List<Item> itemlist = new LinkedList<>();
    private Map<Item, Measure> measureList = new HashMap<>();
    private Map<Item, Double> priceList = new HashMap<>();

    private SingletonBean main;

    protected AddReceiptBB() {
    }

    @PostConstruct
    public void post() {
    }

    @PreDestroy
    public void pre() {
    }

    @Inject
    public AddReceiptBB(SingletonBean main) {
        this.main = main;
    }

    public String getShopname() {
        return shopName;
    }

    public void setShopname(String shopname) {
        this.shopName = shopname;
    }

    public String getNewItemName() {
        return newItemName;
    }

    public void setNewItemName(String newItemName) {
        this.newItemName = newItemName;
    }

    public String getNewItemAmount() {
        return newItemAmount;
    }

    public void setNewItemAmount(String newItemAmount) {
        this.newItemAmount = newItemAmount;
    }

    public String getNewItemUnit() {
        return newItemUnit;
    }

    public void setNewItemUnit(String newItemUnit) {
        this.newItemUnit = newItemUnit;
    }

    public String getNewItemPrice() {
        return newItemPrice;
    }

    public void setNewItemPrice(String newItemPrice) {
        this.newItemPrice = newItemPrice;
    }

    public List<Item> getItemlist() {
        return itemlist;
    }

    public void setItemlist(List<Item> itemlist) {
        this.itemlist = itemlist;
    }

    public Map<Item, Measure> getMeasureList() {
        return measureList;
    }

    public void setMeasureList(Map<Item, Measure> measureList) {
        this.measureList = measureList;
    }

    public Map<Item, Double> getPriceList() {
        return priceList;
    }

    public void setPriceList(Map<Item, Double> priceList) {
        this.priceList = priceList;
    }

    public String getItemUnit(Item item) {
        if (measureList.containsKey(item)) {
            return measureList.get(item).getUnit().toString();
        } else {
            return null;
        }
    }

    public Double getItemAmount(Item item) {
        if (measureList.containsKey(item)) {
            return measureList.get(item).getAmount();
        } else {
            return null;
        }
    }

    public Double getItemPrice(Item item) {
        if (priceList.containsKey(item)) {
            return priceList.get(item);
        } else {
            return null;
        }
    }

//    public void checkShop() {
//        Shop shop = new Shop(shopName, null, user);
//        Shop exist = main.getMain().getShopCatalogue().getByName(shop.getName());
//        if (exist == null) {
//            main.getMain().getShopCatalogue().create(shop);
//        }
//    }
    public void addItem() {
        Item item = new Item(newItemName);
        Item existing = main.getMain().getItemCatalogue().getByName(item.getName());
        if (existing == null) {
            item = main.getMain().getItemCatalogue().update(item);
        } else {
            item = existing;
        }
        itemlist.add(item);
        Measure measure = new Measure(Double.valueOf(newItemAmount), Unit.valueOf(newItemUnit));
        measureList.put(item, measure);
        priceList.put(item, Double.valueOf(newItemPrice));
        setNewItemName("");
        setNewItemAmount("");
        setNewItemUnit("");
        setNewItemPrice("");
    }

    public void removeItem(Item item) {
        itemlist.remove(item);
    }

}
