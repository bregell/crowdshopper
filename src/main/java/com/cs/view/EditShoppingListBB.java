package com.cs.view;

import com.cs.core.SingletonBean;
import com.cs.crowdshopper.Item;
import com.cs.crowdshopper.Measure;
import com.cs.crowdshopper.ShoppingList;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.measure.unit.Unit;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

/**
 *
 * @author iKotsos
 */
@Named
@ViewScoped
public class EditShoppingListBB implements Serializable {

    private long id;
    @Size(min = 2, message = "{name}")
    private String name;
    private SingletonBean main;
    private List<Item> itemlist = new LinkedList<>();
    private Map<Item, Measure> measureList = new HashMap<>();
    @Size(min = 2, message = "{item.name}")
    private String newItemName;
    private String newItemUnit;
    @Min(value = 1, message = "{item.amount}")
    private String newItemAmount;
    private ShoppingList shoppingList;

    protected EditShoppingListBB() {

    }

    @Inject
    public EditShoppingListBB(SingletonBean main) {
        this.main = main;
    }

    public String edit() {
        return "editShoppingList";
    }

    public void init() {
        shoppingList = main.getMain().getShoppingListsCatalogue().find(id);
        if (shoppingList != null) {
            itemlist.addAll(shoppingList.getItems());
            measureList.putAll(shoppingList.getMeasurements());
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Item> getItemlist() {
        return itemlist;
    }

    public void setItemlist(List<Item> itemlist) {
        this.itemlist = itemlist;
    }

    public Map<Item, Measure> getMeasureList() {
        return measureList;
    }

    public void setMeasureList(Map<Item, Measure> measureList) {
        this.measureList = measureList;
    }

    public String getNewItemName() {
        return newItemName;
    }

    public void setNewItemName(String newItemName) {
        this.newItemName = newItemName;
    }

    public String getNewItemUnit() {
        return newItemUnit;
    }

    public void setNewItemUnit(String newItemUnit) {
        this.newItemUnit = newItemUnit;
    }

    public String getNewItemAmount() {
        return newItemAmount;
    }

    public void setNewItemAmount(String newItemAmount) {
        this.newItemAmount = newItemAmount;
    }

    public ShoppingList getShoppingList() {
        return shoppingList;
    }

    public void setShoppingList(ShoppingList shoppingList) {
        this.shoppingList = shoppingList;
    }

    public String getUnit(Item item) {
        if (measureList.containsKey(item)) {
            return measureList.get(item).getUnit().toString();
        } else {
            return main.getMain().getShoppingListsCatalogue().find(id).getMeasurements().get(item).getUnit().toString();
        }
    }

    public Double getAmount(Item item) {
        if (measureList.containsKey(item)) {
            return measureList.get(item).getAmount();
        } else {
            return main.getMain().getShoppingListsCatalogue().find(id).getMeasurements().get(item).getAmount();
        }
    }

    public void addItem() {
        Item item = new Item(newItemName);
        Item existing = main.getMain().getItemCatalogue().getByName(item.getName());
        if (existing == null) {
            item = main.getMain().getItemCatalogue().update(item);
        } else {
            item = existing;
        }
        Measure measure = new Measure(Double.parseDouble(newItemAmount), Unit.valueOf(newItemUnit));
        if(itemlist.contains(item)){
            Measure old = measureList.get(item);
            try{
                measure.add(old);
                measureList.put(item, measure);
            } catch (Exception e) {
                
            }           
        } else {
            measureList.put(item, measure);
            itemlist.add(item);
        }
        newItemName = "";
        newItemAmount = "";
        newItemUnit = "";
    }

    public void removeItem(Item item) {
        itemlist.remove(item);
        measureList.remove(item);
    }
    
    public List<String> getItems(String query){
        List<Item> items = new ArrayList<>(main.getMain().getItemCatalogue().itemSearch(query));
        List<String> itemNames = new LinkedList<>();
        for(Item item: items){
            itemNames.add(item.getName());
        }
        return itemNames;
    }
}
