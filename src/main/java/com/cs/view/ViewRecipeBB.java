package com.cs.view;

import com.cs.core.SingletonBean;
import com.cs.crowdshopper.CSUser;
import com.cs.crowdshopper.Item;
import com.cs.crowdshopper.Recipe;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author iKotsos
 */
@Named
@ViewScoped
public class ViewRecipeBB implements Serializable {

    private long id;
    private Integer portions;
    private String description;
    private String instructions;
    private String name;
    private long author;
    private long userid; 
    private long selectedId;
    private SingletonBean main;
    private CSUser user;
    
    protected  ViewRecipeBB() {   
    }
    
    @Inject
    public ViewRecipeBB(SingletonBean main) {
        this.main = main;
    }
    
    @PostConstruct
    public void post() {
        try {
            user = (CSUser) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("CSUser");
            author = (long) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userid");
        } catch (Exception n){
            
        }
    }

    @PreDestroy
    public void pre() {
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPortions() {
        return portions;
    }

    public String getDescription() {
        return description;
    }

    public String getInstructions() {
        return instructions;
    }
    
    public Collection<Item> getItems() {
        Recipe r = main.getMain().getRecipeCatalogue().find(id);
        Collection<Item> items = r.getItems();
        return items;
    }

    public void setPortions(Integer portions) {
        this.portions = portions;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isOwner() {
        return author == userid;
    }

    public long getAuthor() {
        return author;
    }

    public void setAuthor(long author) {
        this.author = author;
    }

    public long getSelectedId() {
        return selectedId;
    }

    public void setSelectedId(long selectedId) {
        this.selectedId = selectedId;
    }
    
    public String getUnit(Item item) {
        return main.getMain().getRecipeCatalogue().find(id).getMeasurements().get(item).getUnit().toString();
    }
    
    public Double getAmount(Item item) {
        return main.getMain().getRecipeCatalogue().find(id).getMeasurements().get(item).getAmount();
    }
    
    public String isFavorite() {
        Recipe recipe = main.getMain().getRecipeCatalogue().find(id);
        boolean status = main.getMain().getUserCatalogue().getRecipes(user).contains(recipe);
        if (status) {
            return "Unfavorite";
        } else {
            return "Favorite";
        }
    }

    public void favorite() {
        Recipe recipe = main.getMain().getRecipeCatalogue().find(id);
        boolean status = main.getMain().getUserCatalogue().getRecipes(user).contains(recipe);
        if (status) {
            main.getMain().getUserCatalogue().removeRecipe(user, recipe);
        } else {
            main.getMain().getUserCatalogue().addRecipe(user, recipe);
        }
    }
}
