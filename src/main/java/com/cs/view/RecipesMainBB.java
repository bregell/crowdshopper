/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.view;

import com.cs.core.SingletonBean;
import com.cs.crowdshopper.CSUser;
import com.cs.crowdshopper.Item;
import com.cs.crowdshopper.Recipe;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import static jdk.nashorn.internal.codegen.Compiler.LOG;

/**
 *
 * @author iKotsos
 */
@Named
@ViewScoped
public class RecipesMainBB implements Serializable {

    private SingletonBean main;
    private int currentPage;
    private final int pageSize = 8;
    private int count;
    private CSUser user;
  
    protected RecipesMainBB() {
        // Must have for CDI
    }

    @Inject
    public RecipesMainBB(SingletonBean main) {
        this.main = main;
    }

    @PostConstruct
    public void post() {
        count = main.getMain().getRecipeCatalogue().count();
        try{
            user = (CSUser) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("CSUser");
        } catch (Exception n){
            
        }
    }

    public List<Recipe> getList() {
        List<Recipe> recipes = main.getMain().getRecipeCatalogue().findRange(pageSize * currentPage, pageSize);
        LOG.log(Level.INFO, recipes);
        return recipes;
    }
    
    public List<Recipe> getMyList() {
        List<Recipe> recipes = new ArrayList<>(main.getMain().getUserCatalogue().getRecipes(user));
        return recipes;
    }
    
        public List<String> getItemNames() {
        List<Item> items = main.getMain().getItemCatalogue().findAll();
        ArrayList<String> array = new ArrayList<>();
        for (Item temp : items) {
            array.add("\"" + temp.getName() + "\"");
        }
        return array;
    }
    
    public boolean getIsNextDisabled(){
        return ((currentPage + 1) * pageSize >= count);
    }
    
    
    public boolean getIsPreviousDisabled(){
        return currentPage == 0;
    }

    public void next() {
        if ((currentPage + 1) * pageSize < count) {
            currentPage = currentPage + 1;
        }
    }

    public void prev() {
        if (currentPage > 0) {
            currentPage = currentPage - 1;
        }
    }

    // ---- Get/Set -------------
   
    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int count() {
        return count;
    }

    public boolean isOwner(long owner){
        return user.getId().equals(owner);
    }
}
