package com.cs.view;

import com.cs.core.SingletonBean;
import com.cs.crowdshopper.CSUser;
import com.cs.crowdshopper.ShoppingList;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author iKotsos
 */
@Named
@ViewScoped
public class ShoppingListsBB implements Serializable {

    private SingletonBean main;
    private int currentPage;
    private final int pageSize = 8;
    private int count;
    private CSUser user;

    protected ShoppingListsBB() {
        // Must have for CDI
    }

    @Inject
    public ShoppingListsBB(SingletonBean main) {
        this.main = main;
    }

    @PostConstruct
    public void post() {
        count = main.getMain().getShoppingListsCatalogue().count();
        try {
            user = (CSUser) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("CSUser");
        } catch (Exception n){
            
        }
    }

    public List<ShoppingList> getList() {
        if (user != null) {
            List<ShoppingList> shoppinglists = new ArrayList<>(main.getMain().getUserCatalogue().getShoppingLists(user));
            return shoppinglists;
        } else {
            return new LinkedList<>();
        }

    }

    public boolean getIsNextDisabled() {
        return ((currentPage + 1) * pageSize >= count);
    }

    public boolean getIsPreviousDisabled() {
        return currentPage == 0;
    }

    public void next() {
        if ((currentPage + 1) * pageSize < count) {
            currentPage = currentPage + 1;
        }
    }

    public void prev() {
        if (currentPage > 0) {
            currentPage = currentPage - 1;
        }
    }

    // ---- Get/Set -------------
    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int count() {
        return count;
    }

}
