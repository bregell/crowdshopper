/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.view;

import com.cs.crowdshopper.Address;
import com.cs.crowdshopper.CSUser;
import com.cs.crowdshopper.HomeInventory;
import com.cs.crowdshopper.Recipe;
import com.cs.crowdshopper.Shop;
import java.io.Serializable;
import java.util.Collection;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;

/**
 *
 * @author iKotsos
 */
@Named
@RequestScoped
public class EditCSUserBB implements Serializable {

    private long id;
    private String name;
    @Size(min = 4, max = 16, message = "{user.password}")
    private String password;
    @Size(min = 4, max = 16, message = "{user.password}")
    private String newPassword;
    @Size(min = 4, max = 16, message = "{user.password}")
    private String newPassword2;
    @Size(min = 2, message = "{address.street}")
    private String street;
    @Digits(integer = 5, fraction = 0, message = "{address.zipcode1}")
    private int zipcode;
    private HomeInventory inventory;
    private Collection<Shop> shops;
    private Collection<Recipe> recipes;

    public void init() {
        try {
            CSUser author = (CSUser) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("CSUser");
            this.id = author.getId();
            this.name = author.getName();
            this.street = author.getAddress().getStreet();
            this.zipcode = author.getAddress().getZipcode();
        } catch (Exception n) {

        }
    }

    @PostConstruct
    public void post() {
        init();
    }

    @PreDestroy
    public void pre() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getZipcode() {
        return zipcode;
    }

    public void setZipcode(int zipcode) {
        this.zipcode = zipcode;
    }

    public String getPassword() {
        return password;
    }

    public Address getAddress() {
        Address address = new Address(street, zipcode);
        return address;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getNewPassword2() {
        return newPassword2;
    }

    public void setNewPassword2(String newPassword2) {
        this.newPassword2 = newPassword2;
    }

    public HomeInventory getInventory() {
        return inventory;
    }

    public Collection<Shop> getShops() {
        return shops;
    }

    public Collection<Recipe> getRecipes() {
        return recipes;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setInventory(HomeInventory inventory) {
        this.inventory = inventory;
    }

    public void setShops(Collection<Shop> shops) {
        this.shops = shops;
    }

    public void setRecipes(Collection<Recipe> recipes) {
        this.recipes = recipes;
    }

}
