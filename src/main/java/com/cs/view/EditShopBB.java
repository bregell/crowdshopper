/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.view;

import com.cs.crowdshopper.Address;
import com.cs.crowdshopper.ShopInventory;
import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;

/**
 *
 * @author iKotsos
 */
@Named
@RequestScoped
public class EditShopBB implements Serializable {

    private long id;
    @Size(min = 2, message = "{name}")
    private String name;
    @Size(min = 2, message = "{address.street}")
    private String street;
    @Digits(integer = 5, fraction = 0, message = "{address.zipcode1}")
    private int zipcode;
    private ShopInventory inventory;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ShopInventory getInventory() {
        return inventory;
    }

    public void setInventory(ShopInventory inventory) {
        this.inventory = inventory;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getZipcode() {
        return zipcode;
    }

    public void setZipcode(int zipcode) {
        this.zipcode = zipcode;
    }

    public Address getAddress() {
        Address address = new Address(street, zipcode);
        return address;
    }
}
