package com.cs.view;

import com.cs.crowdshopper.Address;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.RequestScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Named;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;

/**
 *
 * @author iKotsos
 */
@Named
@RequestScoped
public class AddCSUserBB implements Serializable {

    @Size(min = 4, message="{user.name}")
    private String name;
    @Size(min = 4, max = 16, message = "{user.password}")
    private String password;
    @Size(min = 2, message="{address.street}")
    private String street;
    @Digits(integer = 5, fraction = 0, message="{address.zipcode1}")
    @Size(min = 5, max = 5, message="{address.zipcode2}")
    private String zipcode;

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    @PostConstruct
    public void post() {
    }

    @PreDestroy
    public void pre() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        Address address = new Address(street, Integer.parseInt(zipcode));
        return address;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void actionListener(ActionEvent e) {
    }
}