package com.cs.view;

import com.cs.core.SingletonBean;
import com.cs.crowdshopper.Item;
import com.cs.crowdshopper.Measure;
import com.cs.crowdshopper.Recipe;
import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.measure.unit.Unit;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author iKotsos
 */
@Named
@ViewScoped
public class AddRecipeBB implements Serializable {

    @NotNull(message = "{recipe.portions}")
    private Integer portions;
    @Size(min = 10, message = "{recipe.description}")
    private String description;
    @Size(min = 10, message = "{recipe.instructions}")
    private String instructions;
    @Size(min = 2, message = "{name}")
    private String name;
    private long author;
    private long user;
    private SingletonBean main;
    private List<Item> itemlist = new LinkedList<>();
    private Map<Item, Measure> measureList = new HashMap<>();
    @Size(min = 2, message = "{item.name}")
    private String newItemName;
    private String newItemUnit;
    @Min(value = 1, message = "{item.amount}")
    private String newItemAmount;
    private Recipe recipe;

    protected AddRecipeBB() {
    }

    @PostConstruct
    public void post() {
        try {
            user = (long) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userid");
        } catch (Exception n){
            
        }
    }

    @PreDestroy
    public void pre() {
    }

    @Inject
    public AddRecipeBB(SingletonBean main) {
        this.main = main;
    }

    public void init() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPortions() {
        return portions;
    }

    public String getDescription() {
        return description;
    }

    public String getInstructions() {
        return instructions;
    }

    public List<Item> getItemlist() {
        return itemlist;
    }

    public void setItemlist(List<Item> itemlist) {
        this.itemlist = itemlist;
    }

    public void setPortions(Integer portions) {
        this.portions = portions;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public boolean isOwner() {
        return author == user;
    }

    public long getAuthor() {
        return author;
    }

    public void setAuthor(long author) {
        this.author = author;
    }

    public String getUnit(Item item) {
        if (measureList.containsKey(item)) {
            return measureList.get(item).getUnit().toString();
        } else {
            return null;
        }
    }

    public Double getAmount(Item item) {
        if (measureList.containsKey(item)) {
            return measureList.get(item).getAmount();
        } else {
            return null;
        }
    }

    public String getNewItemName() {
        return newItemName;
    }

    public void setNewItemName(String newItemName) {
        this.newItemName = newItemName;
    }

    public void setNewItemUnit(String newItemUnit) {
        this.newItemUnit = newItemUnit;
    }

    public String getNewItemUnit() {
        return this.newItemUnit;
    }

    public String getNewItemAmount() {
        return newItemAmount;
    }

    public void setNewItemAmount(String newItemAmount) {
        this.newItemAmount = newItemAmount;
    }

    public void addItem() {
        Item item = new Item(newItemName);
        Item existing = main.getMain().getItemCatalogue().getByName(item.getName());
        if (existing == null) {
            item = main.getMain().getItemCatalogue().update(item);
        } else {
            item = existing;
        }
        Measure measure = new Measure(Double.parseDouble(newItemAmount), Unit.valueOf(newItemUnit));
        if (itemlist.contains(item)) {
            Measure old = measureList.get(item);
            try {
                measure.add(old);
                measureList.put(item, measure);
            } catch (Exception e) {
            }
        } else {
            measureList.put(item, measure);
            itemlist.add(item);
        }
        newItemName = "";
        newItemAmount = "";
        newItemUnit = "";
    }

    public void removeItem(Item item) {
        itemlist.remove(item);
    }

    public void actionListener(ActionEvent e) {
    }

    public Map<Item, Measure> getMeasureList() {
        return measureList;
    }

    public void setMeasureList(Map<Item, Measure> measureList) {
        this.measureList = measureList;
    }

}
