package com.cs.view;

import com.cs.core.SingletonBean;
import com.cs.crowdshopper.CSUser;
import com.cs.crowdshopper.Shop;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author iKotsos
 */
@Named
@ViewScoped
public class ShopsMainBB implements Serializable {

    private SingletonBean main;
    private int currentPage;
    private final int pageSize = 8;
    private int count;
    private CSUser user;
    private List<Shop> shops = new LinkedList<>();

    protected ShopsMainBB() {
        // Must have for CDI
    }

    @Inject
    public ShopsMainBB(SingletonBean main) {
        this.main = main;
    }

    @PostConstruct
    public void post() {
        shops = main.getMain().getShopCatalogue().findAll();
        try {
            user = (CSUser) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("CSUser");
        } catch (Exception n){
            
        }
    }

    public List<Shop> getShops() {
        return shops;
    }

    public void setShops(List<Shop> shops) {
        this.shops = shops;
    }

    public List<String> getShopNames() {
        List<Shop> shopnames = main.getMain().getShopCatalogue().findAll();
        LinkedList<String> array = new LinkedList<>();
        for (Shop temp : shopnames) {
            array.add("\"" + temp.getName() + "\"");
        }
        return array;
    }

    public boolean getIsNextDisabled() {
        return ((currentPage + 1) * pageSize >= count);
    }

    public boolean getIsPreviousDisabled() {
        return currentPage == 0;
    }

    public void next() {
        if ((currentPage + 1) * pageSize < count) {
            currentPage = currentPage + 1;
        }
    }

    public void prev() {
        if (currentPage > 0) {
            currentPage = currentPage - 1;
        }
    }

    // ---- Get/Set -------------
    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int count() {
        return count;
    }

    public CSUser getUser() {
        return user;
    }

    public void setUser(CSUser user) {
        this.user = user;
    }
}
