/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.crowdshopper;

import com.cs.util.AbstractEntity;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 *
 * @author Johan
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class ItemList extends AbstractEntity {

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    private Collection<Item> items;
    @OneToOne
    private CSUser owner;

    public ItemList() {
        items = new LinkedList<>();
    }

    public ItemList(CSUser author, Collection<Item> items) {
        this.items = items;
    } 

    public CSUser getOwner() {
        return owner;
    }

    public void setOwner(CSUser owner) {
        this.owner = owner;
    }
   
    public void setItems(Collection<Item> items) {
        this.items = items;
    } 

    public void addItem(Item i){
        items.add(i);
    }
    
    public Collection<Item> getItems(){
        return items;
    }
    
    public void removeItem(Item i){
        items.remove(i);
    }

    public int size() {
        return items.size();
    }
}
