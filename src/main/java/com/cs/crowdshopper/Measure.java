/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.crowdshopper;

import java.io.Serializable;
import javax.measure.unit.Unit;
import javax.persistence.Embeddable;


/**
 *
 * @author Johan
 */
@Embeddable
public class Measure implements Serializable{
    
    private Double amount;
    private String unit;   

    public Measure() {
    }

    public Measure(Double amount, Unit unit) {
        this.amount = amount;
        this.unit = unit.toString();
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Unit getUnit() {
        return Unit.valueOf(unit);
    }

    public void setUnit(Unit unit) {
        this.unit = unit.toString();
    } 
    
    public void add(Measure measure) throws Exception{
        if(Unit.valueOf(this.unit).equals(measure.getUnit()))
            this.amount += measure.getAmount();
        else 
            throw new Exception();
    }
}
