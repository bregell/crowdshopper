/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.crowdshopper;

import com.cs.util.AbstractDAO;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Johan
 */
@Stateless
public class ShopCatalogue extends AbstractDAO<Shop, Long> implements IShopCatalogue {

    protected ShopCatalogue(){
        super(Shop.class);
    }
    
    @PersistenceContext(unitName = "com.cs_CrowdShopper2_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    @Override
    public EntityManager getEntityManager() {
        return em;
    }

    @Override
    public void addItem(Shop shop, Item item, Double price) {
        shop.getInventory().addItem(item);
        shop = update(shop);
        shop.getInventory().setPrice(item, price);
        shop = update(shop);   
        Logger.getAnonymousLogger().log(Level.INFO, shop.getInventory().getAvgPrices().toString());
    }

    @Override
    public void removeItem(Shop shop, Item item) {
        shop.getInventory().removeItem(item);
        update(shop);
    }

    @Override
    public void updatePrice(Shop shop, Item item, Double price) {
        shop.getInventory().setPrice(item, price);
        update(shop);
    }

    @Override
    public Shop getByName(String name) {
        TypedQuery<Shop> result = em.createQuery("SELECT s FROM Shop s WHERE s.name = :name", Shop.class)
                .setParameter("name", name);
        try {
            Shop shop = result.getSingleResult();
            return shop;
        } catch (NoResultException n)  {
            return null;
        }
    }

    @Override
    public Set<Shop> shopNameSearch(String name) {
        List<Shop> result = new LinkedList<>();
        result.add(getByName(name));
        TypedQuery<Shop> search1 = em.createQuery("SELECT r FROM Shop r WHERE r.name LIKE ':name%'", Shop.class)
                .setParameter("name", name);
        result.addAll(search1.getResultList());
        TypedQuery<Shop> search2 = em.createQuery("SELECT r FROM Shop r WHERE r.name LIKE '%:name'", Shop.class)
                .setParameter("name", name);
        result.addAll(search2.getResultList());
        return new HashSet<>(result);
    }

    @Override
    public Set<Shop> shopAddressSearch(String address) {
        List<Shop> result = new LinkedList<>();
        TypedQuery<Shop> search1 = em.createQuery("SELECT r FROM Shop r WHERE r.address.street = :street", Shop.class)
                .setParameter("street", address);
        result.addAll(search1.getResultList());
        TypedQuery<Shop> search2 = em.createQuery("SELECT r FROM Shop r WHERE r.address.street LIKE '%:street'", Shop.class)
                .setParameter("street", address);
        result.addAll(search2.getResultList());
        TypedQuery<Shop> search3 = em.createQuery("SELECT r FROM Shop r WHERE r.address.street LIKE ':street%'", Shop.class)
                .setParameter("street", address);
        result.addAll(search3.getResultList());
        return new HashSet<>(result);
    }

    @Override
    public Map<Item,Double> getShoppingListPrice(Shop shop, ShoppingList shoppingList) {
        Map<Item,Double> itemPrice = new HashMap<>();
        for(Item i: shoppingList.getItems()){
            double amount = shoppingList.getMeasurement(i).getAmount();
            itemPrice.put(i, shop.getInventory().getPrice(i)*amount);
        }
        return itemPrice;
    }  

    @Override
    public void addToShopInventory(Shop shop, Collection<Item> items, Map<Item, Double> prices) {
        for(Item item: items){
            Item mergedItem = em.merge(item);
            shop.getInventory().addItem(mergedItem);
            shop.getInventory().setPrice(mergedItem, prices.get(item));
            shop = update(shop);
        }
    }
}
