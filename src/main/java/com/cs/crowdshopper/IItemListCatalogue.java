/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.crowdshopper;

import com.cs.util.IDAO;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Johan
 */
@Local
public interface IItemListCatalogue extends IDAO<ItemList, Long> {  

    //public void addItem(ItemList itemList, Item item);
    //public void removeItem(ItemList itemList, Item item);
    public List<Item> getItems(ItemList itemList);
    
}
