/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.crowdshopper;

import static java.lang.Math.ceil;
import static java.lang.Math.sqrt;
import java.util.Date;
import java.util.Map;
import javax.measure.unit.Unit;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyColumn;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.joda.time.DateTime;
import org.joda.time.Days;

/**
 *
 * @author Johan
 */
@Entity
public class HomeInventory extends ShoppingList {
    
    @ElementCollection
    @MapKeyColumn(name="id")
    @Column(name="amountperday")
    @CollectionTable(joinColumns=@JoinColumn(name="id"))
    private Map<Item, Measure> amountPerDay;
    
    @ElementCollection
    @MapKeyColumn(name="id")
    @Column(name="date")
    @CollectionTable(joinColumns=@JoinColumn(name="id"))
    @Temporal(TemporalType.DATE)
    private Map<Item, Date> lastBought;

    public HomeInventory() {
    
    }
    
    public HomeInventory(CSUser user) {
        this.setOwner(user);
    }

    public Map<Item, Measure> getAmountPerDay() {
        return amountPerDay;
    }

    public void setAmountPerDay(Map<Item, Measure> amountPerDay) {
        this.amountPerDay = amountPerDay;
    }

    public Map<Item, Date> getLastBought() {
        return lastBought;
    }

    public void setLastBought(Map<Item, Date> lastBought) {
        this.lastBought = lastBought;
    }

//    public CSUser getUser() {
//        return user;
//    }
//
//    public void setUser(CSUser user) {
//        this.user = user;
//    }
    
    public Measure getAmountPerDay(Item item){
        return amountPerDay.get(item);
    }
    
    public Date getLastBought(Item item){
        return lastBought.get(item);
    }

    public void addMeasure(Item item, Measure measure, Date today) {
        if(getMeasurements().containsKey(item)){
            updateMeasure(item, measure, today);
        } else {
            setMeasurement(item, measure);
            Double d = measure.getAmount();
            d = ceil(d/7);
            amountPerDay.put(item, new Measure(d, measure.getUnit()));
            lastBought.put(item, today);
        }
    }
    
    public void updateMeasure(Item item, Measure curretMeasure, Date today){
        Unit unit = curretMeasure.getUnit();
        //Get date item was last bought
        Date past = lastBought.get(item);
        //Calculate the difference in days
        int days = Days.daysBetween(new DateTime(past), new DateTime(today)).getDays();
        //Get the current buy frqency
        Double currentFreq = amountPerDay.get(item).getAmount();
        //Calculate the new buy frequency
        Double newFreq = this.getMeasurement(item).getAmount()/days;
        newFreq = ceil(sqrt(newFreq * currentFreq));
        //Set the new date the item was last bought
        lastBought.put(item, today);
        //set the new frequency
        amountPerDay.put(item, new Measure(newFreq, unit));
        //set the last amount
        setMeasurement(item, curretMeasure);
    }

    @Override
    public void removeItem(Item i) {
        lastBought.remove(i);
        amountPerDay.remove(i);
        super.removeItem(i);
    }
    
    
}
