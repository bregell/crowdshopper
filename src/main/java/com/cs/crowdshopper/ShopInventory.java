package com.cs.crowdshopper;

import static java.lang.Math.sqrt;
import java.util.Collection;
import java.util.Map;
import javax.persistence.*;

/**
 *
 * @author Johan
 */
@Entity
public class ShopInventory extends ShoppingList{
    
    @ElementCollection
    @MapKeyColumn(name="id_price")
    @Column(name="price")
    @CollectionTable(joinColumns=@JoinColumn(name="id"))
    private Map<Item, Double> prices;
    @ElementCollection
    @MapKeyColumn(name="id_avg")
    @Column(name="avgPrices")
    @CollectionTable(joinColumns=@JoinColumn(name="id"))
    private Map<Item, Double> avgPrices;
    @OneToOne
    private Shop shop;

    public ShopInventory() {
        
    }
    
    public ShopInventory(Shop shop) {
        this.shop = shop;
    }

    public Map<Item, Double> getPrices() {
        return prices;
    }

    public void setPrices(Map<Item, Double> prices) {
        this.prices = prices;
    }

    public Map<Item, Double> getAvgPrices() {
        return avgPrices;
    }

    public void setAvgPrices(Map<Item, Double> avgPrices) {
        this.avgPrices = avgPrices;
    }
    
    @Override
    public void removeItem(Item i) {
        super.removeItem(i);
        prices.remove(i);
        avgPrices.remove(i);
    }

//    public void addItem(Item i, Double price) {
//        addItem(i);
//        prices.put(i, price);
//        avgPrices.put(i, price);
//    }

    public Double getPrice(Item item) {
        return prices.get(item);
    }
    
    public Double getAvgPrice(Item item) {
        return avgPrices.get(item);
    }

    public void setPrice(Item item, Double newPrice) {
        if(avgPrices.containsKey(item)){
            Double newAvgPrice = sqrt(getAvgPrice(item)*newPrice);
            avgPrices.put(item, newAvgPrice);
            prices.put(item, newPrice);
        } else {
            avgPrices.put(item, newPrice);
            prices.put(item, newPrice);
        }
    }
}
