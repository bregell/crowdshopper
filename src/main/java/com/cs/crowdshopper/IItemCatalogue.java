/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.crowdshopper;

import com.cs.util.IDAO;
import java.util.Set;
import javax.ejb.Local;

/**
 *
 * @author Johan
 */
@Local
public interface IItemCatalogue extends IDAO<Item, Long> {
    
    Item getByName(String name);
    String getName(Item item);
    Set<Item> itemSearch(String name);
}
