/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.crowdshopper;

import com.cs.util.AbstractEntity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Johan
 */
@Entity
public class Item extends AbstractEntity {
    
    @NotNull @Column(unique = true)
    private String name;
    
    public Item(){
        
    }

    public Item(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    } 
    
    public void setName(String name){
        this.name = name;
    }
}
