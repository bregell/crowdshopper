/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.crowdshopper;

import com.cs.util.IDAO;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import javax.ejb.Local;

/**
 *
 * @author Johan
 */
@Local
public interface IShopCatalogue extends IDAO<Shop, Long>{
    
    void addItem(Shop shop, Item item, Double price);
    void removeItem(Shop shop, Item item);
    void updatePrice(Shop shop, Item item, Double price);
    Shop getByName(String name);
    Set<Shop> shopNameSearch(String name);
    Set<Shop> shopAddressSearch(String address);
    Map<Item,Double> getShoppingListPrice(Shop shop, ShoppingList shoppingList);
    void addToShopInventory(Shop shop, Collection<Item> items, Map<Item,Double> prices);
}
