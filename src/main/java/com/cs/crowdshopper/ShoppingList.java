package com.cs.crowdshopper;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ElementCollection;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyColumn;

/**
 *
 * @author Johan
 */
@Entity
public class ShoppingList extends ItemList {
    
    private String name; 
    @ElementCollection
    @MapKeyColumn(name="id")
    @Column(name="measurements")
    @CollectionTable(joinColumns=@JoinColumn(name="id"))
    private Map<Item, Measure> measurements;
    
    @Override
    public void removeItem(Item item){
        measurements.remove(item);
        super.removeItem(item);
    }
    
    public ShoppingList(){
        this.name = DateFormat.getDateInstance().format(new Date());
        this.measurements = new HashMap<>();
    }
    
    public ShoppingList(String name){
        this.name = name;
        this.measurements = new HashMap<>();
    }
    
    public ShoppingList(Map<Item, Measure> measurements){
        this.name = DateFormat.getDateInstance().format(new Date());
        this.measurements = measurements;
    }

    public ShoppingList(String name, Map<Item, Measure> measurements) {
        this.name = name;
        this.measurements = measurements;
    }
       
    public Map<Item, Measure> getMeasurements() {
        return measurements;
    }

    public void setMeasurements(Map<Item, Measure> measurements) {
        this.measurements = measurements;
    }

    public String getName() {
        return name;
    } 
    
    public void setName(String name){
        this.name = name;
    }
     
    public Measure getMeasurement(Item item){
        return measurements.get(item);
    }
    
    public void setMeasurement(Item item, Measure measure) {
            measurements.put(item, measure);       
    }
}