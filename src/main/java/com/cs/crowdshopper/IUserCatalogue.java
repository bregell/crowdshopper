/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.crowdshopper;

import com.cs.util.IDAO;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import javax.ejb.Local;
import org.joda.time.DateTime;

/**
 *
 * @author Johan
 */
@Local
public interface IUserCatalogue extends IDAO<CSUser, Long>{
    
    void addShop(CSUser user, Shop shop);
    void removeShop(CSUser user, Shop shop);
    void addRecipe(CSUser user, Recipe Recipe);
    void removeRecipe(CSUser user, Recipe Recipe);
    void changeAddress(CSUser user, String address, Integer zipcode);
    void changeAddress(CSUser user, String street, String number, Integer zipcode);
    void changePassword(CSUser user, String oldPassword, String newPassword);
    Set<Recipe> getRecipes(CSUser user);
    void addShoppingList(CSUser user, ShoppingList shoppingList);
    void removeShoppingList(CSUser user, ShoppingList shoppingList);
    Set<ShoppingList> getShoppingLists(CSUser user);
    ShoppingList generateShoppingList(CSUser user, ShoppingList shoppingList);
    void addToHomeInventory(CSUser user, DateTime date, Collection<Item> items, Map<Item,Measure> measures);
}
