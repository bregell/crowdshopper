/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.crowdshopper;

import com.cs.util.AbstractEntity;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import javax.imageio.ImageIO;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Johan
 */
@Entity
public class Shop extends AbstractEntity {
    
    @NotNull @Column(unique = true, nullable = false)
    private String name;
    @NotNull @Embedded
    private Address address;
    @OneToOne(mappedBy = "shop", cascade = CascadeType.ALL)
    private ShopInventory inventory;
    @OneToOne
    private CSUser owner;
    

    public Shop(){
        
    }
    public Shop(String name, Address address) {
        this.name = name;
        this.address = address;
        this.inventory = new ShopInventory(this);
    }
    
    public Shop(String name, Address address, CSUser owner){
        this.name = name;
        this.address = address;
        this.inventory = new ShopInventory(this);
        this.owner = owner;
    }

    public String getName() {
        return name;
    }

    public Address getAddress() {
        return address;
    }
    
    public ShopInventory getInventory() {
        return inventory;
    }  

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public void setInventory(ShopInventory inventory) {
        this.inventory = inventory;
    }
    
    public Map<Item,Double> getShoppingListPrice(ShoppingList shoppingList) {
        Map<Item,Double> itemPrice = new HashMap<>();
        for(Item i: shoppingList.getItems()){
            itemPrice.put(i, inventory.getPrice(i));
        }
        return itemPrice;
    }

    public CSUser getOwner() {
        return owner;
    }

    public void setOwner(CSUser owner) {
        this.owner = owner;
    }
}
