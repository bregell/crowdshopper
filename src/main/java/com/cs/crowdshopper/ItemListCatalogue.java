/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.crowdshopper;

import com.cs.util.AbstractDAO;
import java.util.LinkedList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class ItemListCatalogue extends AbstractDAO<ItemList, Long> implements IItemListCatalogue {

    @PersistenceContext(unitName = "com.cs_CrowdShopper2_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    @Override
    public EntityManager getEntityManager() {
        return em;
    }

    public ItemListCatalogue() {
        super(ItemList.class);
    }

    @Override
    public List<Item> getItems(ItemList itemList) {
        return new LinkedList<>(itemList.getItems());
    }
}
