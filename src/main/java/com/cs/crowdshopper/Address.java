package com.cs.crowdshopper;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Johan
 */
@Embeddable
public class Address implements Serializable{  
    
    @NotNull
    private String street;
    private String number;
    @NotNull
    private int zipcode;
    
    public Address(){
        
    }
    public Address(String street, int zipcode){
        this.street = street;
        this.zipcode = zipcode;
    }
    
    public Address(String street, String number, int zipcode){
        this.street = street;
        this.number = number;
        this.zipcode = zipcode;
    }

    public String getStreet() {
        return street;
    }

    public int getZipcode() {
        return zipcode;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setZipcode(int zipcode) {
        this.zipcode = zipcode;
    }
    
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
