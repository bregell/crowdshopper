/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.crowdshopper;

import com.cs.util.IDAO;
import java.util.Set;
import javax.ejb.Local;

/**
 *
 * @author iKotsos
 */

@Local
public interface IRecipeCatalogue extends IDAO<Recipe, Long> {

    Set<Recipe> recipeSearch(String name);
    Recipe getByName(String name);

}
