/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.crowdshopper;

import com.cs.util.IDAO;
import java.util.Map;
import javax.ejb.Local;

/**
 *
 * @author Johan
 */
@Local
public interface IShoppingListCatalogue extends IDAO<ShoppingList, Long>{
   
    void addItem(ShoppingList shoppingList, Item item, Measure measure);
    void removeItem(ShoppingList shoppingList, Item item);
    void changeAmount(ShoppingList shoppingList, Item item, Measure amount);
    void addRecipe(ShoppingList shoppingList, Recipe recipe);
    void removeRecipe(ShoppingList shoppingList, Recipe recipe);
    void addHomeInventory(ShoppingList shoppingList, HomeInventory homeInventory);
    void removeHomeInventory(ShoppingList shoppingList, HomeInventory homeInventory);
    Map<Shop,Map<Item,Double>> getCheapestShops(ShoppingList shoppingList);
    
}
