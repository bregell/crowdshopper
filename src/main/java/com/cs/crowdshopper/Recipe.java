package com.cs.crowdshopper;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author Johan
 */
@Entity
public class Recipe extends ShoppingList {

    private Integer portions;
    private String description;
    private String instructions;

    public Recipe() {
    }
    
    public Recipe(String name, Integer portions, String description, String instructions) {
        this.setName(name);
        this.portions = portions;
        this.description = description;
        this.instructions = instructions;
    }

    public Recipe(String name, Integer portions, String description, String instructions, CSUser author) {
        this.setName(name);
        this.portions = portions;
        this.description = description;
        this.instructions = instructions;
        this.setOwner(author);
    }

    public String getDescription() {
        return description;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public Integer getPortions() {
        return portions;
    }

    public void setPortions(Integer portions) {
        this.portions = portions;
    }
}
