/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.crowdshopper;

import com.cs.util.AbstractDAO;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Johan
 */
@Stateless
public class ItemCatalogue extends AbstractDAO<Item, Long> implements IItemCatalogue {

    @PersistenceContext(unitName = "com.cs_CrowdShopper2_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    public EntityManager getEntityManager() {
        return em;
    }

    public ItemCatalogue() {
        super(Item.class);
    }

    @Override
    public Item getByName(String name) {
        TypedQuery<Item> found = em.createQuery("SELECT i FROM Item i WHERE i.name = :name", Item.class)
                .setParameter("name", name);
        try {
            Item item = found.getSingleResult();
            return item;
        } catch (NoResultException n)  {
            return null;
        }
    }

    @Override
    public String getName(Item item
    ) {
        return item.getName();
    }

    @Override
    public Set<Item> itemSearch(String name
    ) {
        List<Item> result = new LinkedList<>();
        result.add(getByName(name));
        TypedQuery<Item> search1 = em.createQuery("SELECT r FROM Item r WHERE r.name LIKE ':name%'", Item.class)
                .setParameter("name", name);
        result.addAll(search1.getResultList());
        TypedQuery<Item> search2 = em.createQuery("SELECT r FROM Item r WHERE r.name LIKE '%:name'", Item.class)
                .setParameter("name", name);
        result.addAll(search2.getResultList());
        return new HashSet<>(result);
    }
}
