package com.cs.crowdshopper;

import com.cs.util.AbstractDAO;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author iKotsos
 */
@Stateless
public class RecipeCatalogue extends AbstractDAO<Recipe, Long> implements IRecipeCatalogue {

    @PersistenceContext(unitName = "com.cs_CrowdShopper2_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    public EntityManager getEntityManager() {
        return em;
    }

    public RecipeCatalogue() {
        super(Recipe.class);
    }

    @Override
    public void delete(Long id) {
        em.createNativeQuery("DELETE FROM app.CSUSER_RECIPE WHERE FAVORITERECIPES_ID = "+id.toString()+"")
                //Query("DELETE FROM CSUser AS c WHERE C.favoriteRecipes.")
                //.setParameter("id", id)
                .executeUpdate();
        super.delete(id);
    }

    @Override
    public Set<Recipe> recipeSearch(String name) {
        List<Recipe> result = new LinkedList<>();
        result.add(getByName(name));
        TypedQuery<Recipe> search1 = em.createQuery("SELECT r FROM Recipe r WHERE r.name LIKE ':name%'", Recipe.class)
                .setParameter("name", name);
        result.addAll(search1.getResultList());
        TypedQuery<Recipe> search2 = em.createQuery("SELECT r FROM Recipe r WHERE r.name LIKE '%:name'", Recipe.class)
                .setParameter("name", name);
        result.addAll(search2.getResultList());
        return new HashSet<>(result);
    }

    @Override
    public Recipe getByName(String name) {
        TypedQuery<Recipe> found = em.createQuery("SELECT r FROM Recipe r WHERE r.name = :name", Recipe.class)
                .setParameter("name", name);
        try {
            Recipe recipe = found.getSingleResult();
            return recipe;
        } catch (NoResultException n) {
            return null;
        }
    }
}
