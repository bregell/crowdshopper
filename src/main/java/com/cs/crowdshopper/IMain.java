/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.crowdshopper;

import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.inject.Named;

/**
 *
 * @author Johan
 */
@Named
@Local
public interface IMain {

    public IItemCatalogue getItemCatalogue();
    
    public IRecipeCatalogue getRecipeCatalogue();
    
    public IShopCatalogue getShopCatalogue();

    public IShoppingListCatalogue getShoppingListsCatalogue();

    public IUserCatalogue getUserCatalogue();

    public IItemListCatalogue getItemListCatalogue();
    
}
