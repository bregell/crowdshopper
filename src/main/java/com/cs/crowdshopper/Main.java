/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.crowdshopper;

import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Johan
 */
@Stateless
public class Main implements IMain {

    @EJB
    private IItemCatalogue itemCatalogue;
    @EJB
    private IRecipeCatalogue recipeCatalogue;
    @EJB
    private IShopCatalogue shopCatalogue;
    @EJB
    private IShoppingListCatalogue shoppingListCatalogue;
    @EJB
    private IUserCatalogue userCatalogue;
    @EJB
    private IItemListCatalogue itemListCatalogue;

    /**
     * Creates a new instance of Main
     */
    public Main() {

    }

    @Override
    public IItemCatalogue getItemCatalogue() {
        return itemCatalogue;
    }

    @Override
    public IRecipeCatalogue getRecipeCatalogue() {
        return recipeCatalogue;
    }

    @Override
    public IShopCatalogue getShopCatalogue() {
        return shopCatalogue;
    }

    @Override
    public IShoppingListCatalogue getShoppingListsCatalogue() {
        return shoppingListCatalogue;
    }

    @Override
    public IUserCatalogue getUserCatalogue() {
        return userCatalogue;
    }

    @Override
    public IItemListCatalogue getItemListCatalogue() {
        return itemListCatalogue;
    }
}
