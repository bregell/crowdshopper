/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.crowdshopper;

import com.cs.util.AbstractDAO;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnitUtil;
import javax.persistence.TypedQuery;

/**
 *
 * @author Johan
 */
@Stateless
public class ShoppingListCatalogue extends AbstractDAO<ShoppingList, Long> implements IShoppingListCatalogue{

    protected ShoppingListCatalogue(){
             super(ShoppingList.class);
    }

    @PersistenceContext(unitName = "com.cs_CrowdShopper2_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    @Override
    public EntityManager getEntityManager() {
        return em;
    }
    
    @Override
    public void addItem(ShoppingList shoppingList, Item item, Measure measure){
        if(shoppingList.getId() == null){
            create(shoppingList);
            em.refresh(shoppingList);
        } else {
            if(item.getId() != null){
                shoppingList.addItem(item);
                update(shoppingList);
                em.refresh(shoppingList);
                em.refresh(item);
                shoppingList.setMeasurement(item, measure);
                update(shoppingList);
            } else {
                shoppingList.addItem(item);
                update(shoppingList);
                em.refresh(item);
                shoppingList.setMeasurement(item, measure);
                update(shoppingList);
            }
        }
    }

    @Override
    public void removeItem(ShoppingList shoppingList, Item item) {
        shoppingList.removeItem(item);
        update(shoppingList);
    }

    @Override
    public void changeAmount(ShoppingList shoppingList, Item item, Measure amount) {
        shoppingList.setMeasurement(item, amount);
        update(shoppingList);
    }

    @Override
    public void addRecipe(ShoppingList shoppingList, Recipe recipe) {
        for(Item item : recipe.getItems()){
            Measure recipeMeasure = recipe.getMeasurement(item);
            if(shoppingList.getItems().contains(item)){
                Measure currentMeasure = shoppingList.getMeasurement(item);
                Double d = currentMeasure.getAmount() + recipeMeasure.getAmount();
                shoppingList.setMeasurement(item, new Measure(d, currentMeasure.getUnit()));
            } else {
                shoppingList.addItem(item);
                shoppingList = update(shoppingList);
                shoppingList.setMeasurement(item, recipeMeasure);  
            }
            shoppingList = update(shoppingList);
        }
        
    }

    @Override
    public void removeRecipe(ShoppingList shoppingList, Recipe recipe) {
        for(Item item : recipe.getItems()){
            Measure itemMeasure = recipe.getMeasurement(item);
            if(shoppingList.getItems().contains(item)){
                Measure currentMeasure = shoppingList.getMeasurement(item);
                Double d 
                        = currentMeasure.getAmount()
                        - itemMeasure.getAmount()
                ;
                Measure newMeasure = new Measure(d, currentMeasure.getUnit());
                if(newMeasure.getAmount() >= 0) {
                    shoppingList.setMeasurement(item, newMeasure);
                } else {
                    shoppingList.removeItem(item);
                }
                
            }
        }
    }

    @Override
    public void addHomeInventory(ShoppingList shoppingList, HomeInventory homeInventory) {
        for(Item item : homeInventory.getItems()){
            Measure homeMeasure = homeInventory.getMeasurement(item);
            if(shoppingList.getItems().contains(item)){
                Measure newMeasure = measureAdd(homeMeasure, shoppingList.getMeasurement(item));
                shoppingList.setMeasurement(item, newMeasure);
            } else {
                shoppingList.addItem(item);
                shoppingList.setMeasurement(item, homeMeasure);
            }
        }
    }

    @Override
    public void removeHomeInventory(ShoppingList shoppingList, HomeInventory homeInventory) {
        for(Item item : homeInventory.getItems()){
            if(shoppingList.getItems().contains(item)){
                Measure newMeasure = measureSub(shoppingList.getMeasurement(item), homeInventory.getMeasurement(item));
                if(newMeasure.getAmount() >= 0) {
                    shoppingList.setMeasurement(item, newMeasure);
                } else {
                    shoppingList.removeItem(item);
                }
                
            }
        }
    }  
    
    private Measure measureAdd(Measure a, Measure b){
        return new Measure(a.getAmount() + b.getAmount(), a.getUnit());
    }
    
    private Measure measureSub(Measure a, Measure b){
        return new Measure(a.getAmount() - b.getAmount(), a.getUnit());
    }

    @Override
    public Map<Shop,Map<Item,Double>> getCheapestShops(ShoppingList shoppingList) {
        TypedQuery<Shop> query = em.createQuery("SELECT s FROM Shop s", Shop.class);
        List<Shop> shops = query.getResultList();
        Map<Shop,Map<Item,Double>> priceList = new HashMap<>();
        for(Shop s: shops){
            priceList.put(s, s.getShoppingListPrice(shoppingList));
        }
        return priceList;
    }  
}
