/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.crowdshopper;

import com.cs.util.AbstractEntity;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.Constraint;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Johan
 */
@Entity
public class CSUser extends AbstractEntity {

    @NotNull @Column(unique = true, nullable = false)
    private String name;
    @NotNull @Column(nullable = false)
    private String password;
    @Embedded 
    private Address address;
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    private Collection<Shop> favoriteShops;
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    private Collection<Recipe> favoriteRecipes;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Collection<ShoppingList> shoppinglists;
    @OneToOne(mappedBy = "owner", cascade = CascadeType.ALL)
    private HomeInventory inventory;

    public CSUser() {
        this.inventory = new HomeInventory(this);
    }

    public CSUser(String name, String password, Address address) {
        this.inventory = new HomeInventory(this);
        this.name = name;
        this.password = password;
        this.address = address;     
    }

    public String getName() {
        return name;
    }

    public Address getAddress() {
        return address;
    }

    public Collection<Shop> getFavoriteShops() {
        return favoriteShops;
    }

    public Collection<Recipe> getFavoriteRecipes() {
        return favoriteRecipes;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public HomeInventory getInventory() {
        return inventory;
    }

    public void setInventory(HomeInventory inventory) {
        this.inventory = inventory;
    }

    void addShop(Shop shop) {
        this.favoriteShops.add(shop);
    }

    void removeShop(Shop shop) {
        this.favoriteShops.remove(shop);
    }

    void addRecipe(Recipe Recipe) {
        this.favoriteRecipes.add(Recipe);
    }

    void removeRecipe(Recipe Recipe) {
        this.favoriteRecipes.remove(Recipe);
    }

    public Collection<ShoppingList> getShoppinglists() {
        return shoppinglists;
    }

    public void setShoppinglists(Collection<ShoppingList> shoppinglists) {
        this.shoppinglists = shoppinglists;
    }

    public void addShoppingList(ShoppingList shoppingList) {
        this.shoppinglists.add(shoppingList);
    }

    public void removeShoppingList(ShoppingList shoppingList) {
        this.shoppinglists.remove(shoppingList);
    }
    
}
