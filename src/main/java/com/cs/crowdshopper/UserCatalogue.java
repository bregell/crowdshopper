package com.cs.crowdshopper;

import com.cs.util.AbstractDAO;
import static java.lang.Math.ceil;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.joda.time.DateTime;
import org.joda.time.Days;

/**
 *
 * @author Johan
 */
@Stateless
public class UserCatalogue extends AbstractDAO<CSUser, Long> implements IUserCatalogue {

    protected UserCatalogue() {
        super(CSUser.class);
    }

    @PersistenceContext(unitName = "com.cs_CrowdShopper2_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    public EntityManager getEntityManager() {
        return em;
    }

    @Override
    public void create(CSUser t) {
        //HomeInventory inventory = em.create(new HomeInventory(t));
        //t.setInventory(null);)
        super.create(t);
    }

    @Override
    public void changePassword(CSUser user, String oldPassword, String newPassword) {
        if (user.getPassword().equals(oldPassword)) {
            user.setPassword(newPassword);
            update(user);
        }
    }

    @Override
    public void addShop(CSUser user, Shop shop) {
        user.addShop(shop);
        update(user);
    }

    @Override
    public void removeShop(CSUser user, Shop shop) {
        user.removeShop(shop);
        update(user);
    }

    @Override
    public void addRecipe(CSUser user, Recipe recipe) {
        if (!user.getFavoriteRecipes().contains(recipe)) {
            user.addRecipe(recipe);
            update(user);
        }
    }

    @Override
    public void removeRecipe(CSUser user, Recipe recipe) {
        if (user.getFavoriteRecipes().contains(recipe)) {
            user.removeRecipe(recipe);
            update(user);
        }
    }

    @Override
    public void changeAddress(CSUser user, String address, Integer zipcode) {
        user.setAddress(new Address(address, zipcode));
        update(user);
    }

    @Override
    public void changeAddress(CSUser user, String address, String number, Integer zipcode) {
        user.setAddress(new Address(address, number, zipcode));
        update(user);
    }

    @Override
    public Set<Recipe> getRecipes(CSUser user) {
        TypedQuery<Recipe> query = em.createQuery("SELECT r FROM Recipe r WHERE r.owner = :user", Recipe.class)
                .setParameter("user", user);
        Set<Recipe> result = new HashSet<>(query.getResultList());
        result.addAll(user.getFavoriteRecipes());
        return result;
    }

    @Override
    public void addShoppingList(CSUser user, ShoppingList shoppingList) {
        user.addShoppingList(shoppingList);
        update(user);
    }

    @Override
    public void removeShoppingList(CSUser user, ShoppingList shoppingList) {
        user.removeShoppingList(shoppingList);
        update(user);
    }

    @Override
    public Set<ShoppingList> getShoppingLists(CSUser user) {
        em.flush();
        return new HashSet<>(user.getShoppinglists());
    }

    @Override
    public ShoppingList generateShoppingList(CSUser user, ShoppingList shoppingList) {
        HomeInventory inventory = user.getInventory();
        shoppingList.setOwner(user);
        for(Item i : inventory.getItems()){
            Measure amount = inventory.getAmountPerDay(i);
            int days = Days.daysBetween(new DateTime(inventory.getLastBought(i)), new DateTime()).getDays();
            if(days >= amount.getAmount()){
                Double d = ceil(amount.getAmount()*days);
                shoppingList.addItem(i);
                shoppingList.setMeasurement(i, new Measure(d, amount.getUnit()));
            }  
        }
        user.addShoppingList(shoppingList);
        update(user);
        return shoppingList;
    }

    @Override
    public void addToHomeInventory(CSUser user, DateTime date, Collection<Item> items, Map<Item, Measure> measures) {
        for (Item item : items) {
            Item mergedItem = em.merge(item);
            user.getInventory().addItem(mergedItem);
            user.getInventory().addMeasure(mergedItem, measures.get(item), date.toDate());
            user = update(user);
        }
    }
}
