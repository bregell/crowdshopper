package com.cs.util;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

/**
 * A container for entities, base class for OrderBook, ProductCatalogue,
 * CustomerRegistry The fundamental common operations are here 
 * (Create, Read, Update, Delete = CRUD).
 *
 * T is type for items in container K is type of id (primary key)
 *
 * @author hajo
 * @param <T> Any type
 * @param <K> Key
 */
public abstract class AbstractDAO<T,K> implements IDAO<T,K>{
    
    private final Class<T> clazz;
    
    protected AbstractDAO(Class<T> clazz){
        this.clazz = clazz;
    }
    
    protected abstract EntityManager getEntityManager();

    @Override
    public void create(T t) {
        getEntityManager().persist(t);
        getEntityManager().flush();
    }


    @Override
    public void delete(K id) {
        getEntityManager().remove(getEntityManager().getReference(clazz, id));
        getEntityManager().flush();
    }

    @Override
    public T update(T t) {
        T result = getEntityManager().merge(t);
        getEntityManager().flush();
        return result;
        
    }

    @Override
    public T find(K id) {
        getEntityManager().flush();
        return getEntityManager().find(clazz, id);
    }

    @Override
    public List<T> findAll() {
        getEntityManager().flush();
        String queryClass = clazz.getSimpleName();
        List<T> list = getEntityManager()
                .createQuery("SELECT c FROM "+queryClass+" c")
                .getResultList();
        return list;
    }

    @Override
    public List<T> findRange(int first, int amount) {
        getEntityManager().flush();
        String queryClass = clazz.getSimpleName();
        List<T> list = getEntityManager()
                .createQuery("SELECT c FROM "+queryClass+" c")
                .setFirstResult(first)
                .setMaxResults(amount)
                .getResultList();
        return list;
    }

    @Override
    public int count() {
        getEntityManager().flush();
        String queryClass = clazz.getSimpleName();
        long count = getEntityManager()
                .createQuery("SELECT count(c) FROM "+queryClass+" c", Long.class)
                .getSingleResult();
        try{
            return (int) count;
        } catch(NoResultException n){
            return 0;
        }
    }
}
