package com.cs.ctrl;

import com.cs.core.SingletonBean;
import com.cs.crowdshopper.CSUser;
import com.cs.crowdshopper.ShoppingList;
import com.cs.view.AddShoppingListBB;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author iKotsos
 */
@Named
@RequestScoped
public class AddShoppingListCtrl implements Serializable {

    private SingletonBean main;
    private AddShoppingListBB nslistBB;
    CSUser author;

    protected AddShoppingListCtrl() {
        // Must have for CDI
    }

    @PostConstruct
    public void post() {
        try {
            author = (CSUser) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("CSUser");
        } catch (Exception n){
            
        }
    }

    @PreDestroy
    public void pre() {
    }

    @Inject
    public AddShoppingListCtrl(SingletonBean main) {
        this.main = main;
    }

    @Inject
    public void setShoppingListBB(AddShoppingListBB nslistBB) {
        this.nslistBB = nslistBB;
    }
    

    public String save() {
        ShoppingList slist = new ShoppingList();
        slist.setItems(nslistBB.getItemlist());
        slist.setMeasurements(nslistBB.getMeasureList());
        slist.setName(nslistBB.getName());
        slist.setOwner(author);
        slist = main.getMain().getShoppingListsCatalogue().update(slist);
        main.getMain().getUserCatalogue().addShoppingList(author, slist);
        return "myShoppingLists?faces-redirect=true";
    }
}
