package com.cs.ctrl;

import com.cs.core.SingletonBean;
import com.cs.crowdshopper.CSUser;
import com.cs.crowdshopper.Recipe;
import com.cs.view.AddRecipeBB;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author iKotsos
 */
@Named
@RequestScoped
public class AddRecipeCtrl implements Serializable {

    private SingletonBean main;
    private AddRecipeBB nrecipeBB;
    CSUser author;

    protected AddRecipeCtrl() {
        // Must have for CDI
    }

    @PostConstruct
    public void post() {
        try {
            author = (CSUser) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("CSUser");
        } catch (Exception n){
            
        }
    }

    @PreDestroy
    public void pre() {
    }

    @Inject
    public AddRecipeCtrl(SingletonBean main) {
        this.main = main;
    }

    @Inject
    public void setRecipeBB(AddRecipeBB nrecipeBB) {
        this.nrecipeBB = nrecipeBB;
    }

    public String save() {
        Recipe recipe = new Recipe(
                nrecipeBB.getName(),
                nrecipeBB.getPortions(),
                nrecipeBB.getDescription(),
                nrecipeBB.getInstructions(),
                author
        );
        recipe.setItems(nrecipeBB.getItemlist());
        recipe.setMeasurements(nrecipeBB.getMeasureList());
        main.getMain().getRecipeCatalogue().update(recipe);
        return "recipesMain?faces-redirect=true";
    }

}
