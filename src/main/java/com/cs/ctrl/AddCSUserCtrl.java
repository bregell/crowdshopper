package com.cs.ctrl;

import com.cs.core.SingletonBean;
import com.cs.crowdshopper.CSUser;
import com.cs.view.AddCSUserBB;
import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author iKotsos
 */
@Named
@RequestScoped
public class AddCSUserCtrl implements Serializable {

    private SingletonBean main;
    private AddCSUserBB nuserBB;

    protected AddCSUserCtrl() {
        // Must have for CDI
    }

    @Inject
    public AddCSUserCtrl(SingletonBean main) {
        this.main = main;
    }
    
    @Inject
    public void setCSUserBB(AddCSUserBB nuserBB) {
        this.nuserBB = nuserBB;
    }
   
    public String save() {
        CSUser user = new CSUser (
                nuserBB.getName(),
                nuserBB.getPassword(), 
                nuserBB.getAddress() 
        );
        main.getMain().getUserCatalogue().create(user);
        return "/index?faces-redirect=true";
    }
}