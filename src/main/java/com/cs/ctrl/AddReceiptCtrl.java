/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.ctrl;

import com.cs.core.SingletonBean;
import com.cs.crowdshopper.CSUser;
import com.cs.crowdshopper.Item;
import com.cs.crowdshopper.Shop;
import com.cs.view.AddReceiptBB;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Rober
 */
@Named
@RequestScoped
public class AddReceiptCtrl implements Serializable {

    private SingletonBean main;
    private AddReceiptBB receiptBB;

    CSUser user;

    protected AddReceiptCtrl() {
        // Must have for CDI
    }

    @PostConstruct
    public void post() {
        try {
            user = (CSUser) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("CSUser");
        } catch (Exception n){
            
        }
    }

    @PreDestroy
    public void pre() {
    }

    @Inject
    public AddReceiptCtrl(SingletonBean main) {
        this.main = main;
    }

    @Inject
    public void setReceiptBB(AddReceiptBB receiptBB) {
        this.receiptBB = receiptBB;
    }


    public String save() {
        Shop shop = new Shop(receiptBB.getShopname(), null, user);
        Shop exist = main.getMain().getShopCatalogue().getByName(shop.getName());
        if (exist == null) {
            shop = main.getMain().getShopCatalogue().update(shop);
        }
        else {
            shop = exist;
        }        
        List<Item> itemlist = receiptBB.getItemlist();
        for (Item item : itemlist) {
            Double price = receiptBB.getItemPrice(item);
            main.getMain().getShopCatalogue().addItem(shop, item, price);
        }
        return "/home?faces-redirect=true";

    }
}
