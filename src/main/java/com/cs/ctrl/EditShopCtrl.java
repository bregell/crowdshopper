/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.ctrl;

import com.cs.core.SingletonBean;
import com.cs.crowdshopper.Shop;
import com.cs.view.EditShopBB;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author iKotsos
 */
@Named
@RequestScoped
public class EditShopCtrl implements Serializable {

    private SingletonBean main;
    private EditShopBB shopBB;

    protected EditShopCtrl() {
        // Must have for CDI
    }

    @PostConstruct
    public void post() {
    }

    @PreDestroy
    public void pre() {
    }

    @Inject
    public EditShopCtrl(SingletonBean main) {
        this.main = main;
    }

    @Inject
    public void setEditShopBB(EditShopBB shopBB) {
        this.shopBB = shopBB;
    }

    public String update() {
        Shop shop = main.getMain().getShopCatalogue().find(shopBB.getId());
        shop.setName(shopBB.getName());
        shop.setAddress(shopBB.getAddress());
        main.getMain().getShopCatalogue().update(shop);
        return "shopsMain?faces-redirect=true";
    }

    public String delete() {
        main.getMain().getShopCatalogue().delete(shopBB.getId());
        return "shopsMain?faces-redirect=true";
    }
}
