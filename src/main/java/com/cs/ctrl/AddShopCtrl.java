/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.ctrl;

import com.cs.core.SingletonBean;
import com.cs.crowdshopper.CSUser;
import com.cs.crowdshopper.Shop;
import com.cs.view.AddShopBB;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author iKotsos
 */
@Named
@RequestScoped
public class AddShopCtrl implements Serializable {

    private SingletonBean main;
    private AddShopBB nshopBB;
    CSUser user;

    protected AddShopCtrl() {
        // Must have for CDI
    }

    @PostConstruct
    public void post() {
        try {
            user = (CSUser) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("CSUser");
        } catch (Exception n){
            
        }
    }

    @PreDestroy
    public void pre() {
    }

    @Inject
    public AddShopCtrl(SingletonBean main) {
        this.main = main;
    }
    
    @Inject
    public void setShopBB(AddShopBB nshopBB) {
        this.nshopBB = nshopBB;
    }
   
    public String save() {
        main.getMain().getShopCatalogue().create(new Shop(nshopBB.getName(), nshopBB.getAddress(), user));
        return "shopsMain?faces-redirect=true";
    }
}