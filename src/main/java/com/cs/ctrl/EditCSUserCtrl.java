/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.ctrl;

import com.cs.core.SingletonBean;
import com.cs.crowdshopper.Address;
import com.cs.crowdshopper.CSUser;
import com.cs.view.EditCSUserBB;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Rober
 */
@Named
@RequestScoped
public class EditCSUserCtrl implements Serializable {

    private SingletonBean main;
    private EditCSUserBB csuserBB;

    CSUser author;

    protected EditCSUserCtrl() {
        // Must have for CDI
    }

    @PostConstruct
    public void post() {
        try {
            author = (CSUser) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("CSUser");
        } catch (Exception n){
            
        }
    }

    @PreDestroy
    public void pre() {
    }

    @Inject
    public EditCSUserCtrl(SingletonBean main) {
        this.main = main;
    }

    @Inject
    public void setCSUserBB(EditCSUserBB csuserBB) {
        this.csuserBB = csuserBB;
    }

    public String update() {
        String street = csuserBB.getStreet();
        Integer zip = csuserBB.getZipcode();
        main.getMain().getUserCatalogue().changeAddress(author, street, zip);
        return "shopperProfile?faces-redirect=true";
    }

    public String updatePass() {
        String oldpassword = csuserBB.getPassword();
        String newpassword = csuserBB.getNewPassword();
        String newpassword2 = csuserBB.getNewPassword2();
        try {
            if (newpassword.equals(newpassword2)) {
                main.getMain().getUserCatalogue().changePassword(author, oldpassword, newpassword);
                return "shopperProfile?faces-redirect=true";
            }
        } catch (Exception e) {

        } FacesContext.getCurrentInstance().
                    addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
             "New passwords don't match, please try again!", null));
        return null;
    }

    public String delete() {
        main.getMain().getUserCatalogue().delete(author.getId());
        return "index?faces-redirect=true";
    }
}
