package com.cs.ctrl;

import com.cs.core.SingletonBean;
import com.cs.crowdshopper.CSUser;
import com.cs.crowdshopper.Recipe;
import com.cs.view.EditRecipeBB;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author iKotsos
 */
@Named
@RequestScoped
public class EditRecipeCtrl implements Serializable {

    private SingletonBean main;
    private EditRecipeBB recipeBB;
    CSUser user;

    protected EditRecipeCtrl() {
        // Must have for CDI
    }

    @PostConstruct
    public void post() {
        try {
            user = (CSUser) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("CSUser");
        } catch (Exception n){
            
        }
    }

    @PreDestroy
    public void pre() {
    }

    @Inject
    public EditRecipeCtrl(SingletonBean main) {
        this.main = main;
    }

    @Inject
    public void setEditRecipeBB(EditRecipeBB recipeBB) {
        this.recipeBB = recipeBB;
    }

    public void update() {
        Recipe recipe = main.getMain().getRecipeCatalogue().find(recipeBB.getId());
        recipe.setName(recipeBB.getName());
        recipe.setPortions(recipeBB.getPortions());
        recipe.setDescription(recipeBB.getDescription());
        recipe.setInstructions(recipeBB.getInstructions());
        recipe.setItems(recipeBB.getItemlist());
        recipe.setMeasurements(recipeBB.getMeasureList());
        main.getMain().getRecipeCatalogue().update(recipe);
    }

    

    public String delete() {
        main.getMain().getRecipeCatalogue().delete(recipeBB.getId());
        return "recipesMain?faces-redirect=true";
    }

}
