/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.ctrl;

import com.cs.core.SingletonBean;
import com.cs.crowdshopper.CSUser;
import com.cs.crowdshopper.Recipe;
import com.cs.crowdshopper.ShoppingList;
import com.cs.view.EditShoppingListBB;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Rober
 */
@Named
@RequestScoped
public class EditShoppingListCtrl implements Serializable {

    private SingletonBean main;
    private EditShoppingListBB shoppingListBB;
    CSUser user;

    protected EditShoppingListCtrl() {

    }
    
    @PostConstruct
    public void post() {
        try {
            user = (CSUser) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("CSUser");
        } catch (Exception n){
            
        }
    }

    @Inject
    public EditShoppingListCtrl(SingletonBean main) {
        this.main = main;
    }

    @Inject
    public void setShoppingListBB(EditShoppingListBB shoppingListBB) {
        this.shoppingListBB = shoppingListBB;
    }

    public String addRecipeToList(long recipeId, long shoppingListId) {
        ShoppingList shoppingList = main.getMain().getShoppingListsCatalogue().find(shoppingListId);
        Recipe recipe = main.getMain().getRecipeCatalogue().find(recipeId);
        main.getMain().getShoppingListsCatalogue().addRecipe(shoppingList, recipe);
        return "viewRecipe";
    }

    public void update() {
        ShoppingList shoppingList = main.getMain().getShoppingListsCatalogue().find(shoppingListBB.getId());
        shoppingList.setName(shoppingListBB.getName());
        shoppingList.setItems(shoppingListBB.getItemlist());
        shoppingList.setMeasurements(shoppingListBB.getMeasureList());
        main.getMain().getShoppingListsCatalogue().update(shoppingList);
    }

    public void removeList(ShoppingList shoppingList) {
        main.getMain().getUserCatalogue().removeShoppingList(user, shoppingList);
    }

    public void generate() {
        if (user != null) {
            ShoppingList shoppingList = new ShoppingList();
            shoppingList = main.getMain().getShoppingListsCatalogue().update(shoppingList);
            shoppingList = main.getMain().getUserCatalogue().generateShoppingList(user, shoppingList);
            main.getMain().getShoppingListsCatalogue().update(shoppingList);
        }
    }
}
