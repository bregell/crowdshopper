package com.cs.auth;

import com.cs.core.SingletonBean;
import com.cs.crowdshopper.CSUser;
import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author iKotsos
 */
@Named
@RequestScoped
public class AuthBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @PersistenceContext(unitName = "com.cs_CrowdShopper2_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public EntityManager getEntityManager() {
        return em;
    }
    private SingletonBean main;
    private String name;
    private String password;

    public String login() {
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext externalContext = context.getExternalContext();
        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();

        String flag = "fail";
        TypedQuery<CSUser> q = em.createQuery("SELECT u FROM CSUser u WHERE u.name = :name AND u.password = :password", CSUser.class);
        q.setParameter("name", name);
        q.setParameter("password", password);
        try {
            CSUser user = (CSUser) q.getSingleResult();
            if (name.equalsIgnoreCase(user.getName()) && password.equals(user.getPassword())) {
                flag = "success";
                externalContext.getSessionMap().put("username", user.getName());
                externalContext.getSessionMap().put("userid", user.getId());
                externalContext.getSessionMap().put("CSUser", user);
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().
                    addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_WARN,
                                    "Login Failed, Please check your Username/Password and try again", null));
            externalContext.getFlash().setKeepMessages(true);

        }
        return flag;
    }

    public String logout() {
        ExternalContext externalContext = FacesContext.getCurrentInstance().
                getExternalContext();
        externalContext.invalidateSession();
        return "/index?faces-redirect=true";
    }

    // ------------------------------
    // Getters & Setters 
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }
}
