package com.cs.core;

import com.cs.crowdshopper.IMain;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.ejb.Singleton;

/**
 *
 * @author iKotsos
 */
@Singleton
public class SingletonBean implements Serializable {

    @EJB
    private IMain main;

    public IMain getMain() {
        return main;
    }
}
